# **Projeto IOT - Checkpoint 01**

Felipe Tiago De Carli - 10525686

Rafael Gongora Bariccatti - 10892273

## 1 - Aplicação - Definição de linguagem e tecnologia

Para a escolha das tecnologias da camada de aplicação, deve ser considerada uma tecnologia que se comunica com o microsserviço definido, e se necessário com o banco de dados.

Conforme a sugestão disponibilizada no documento de especificação do projeto, será utilizado o stack **HTML** , **CSS** e **JS** , para que seja possível construir a camada de aplicação em uma interface web interativa.

O HTML é uma linguagem de marcação, utilizada para a construção de páginas Web. Essa é uma linguagem utilizada globalmente, com diversas ferramentas e com uma portabilidade gigantesca.

O CSS é uma linguagem para desenvolvimento de layout, quase sempre utilizada junto ao HTML. Com o CSS, é possível definir a &quot;cara&quot; da página da WEB, realizando inserções de cores, animações e estruturas de posicionamento bem definidas.

O Javascript (JS) é uma linguagem de programação voltada para o desenvolvimento Web. Essa linguagem é utilizada por todos os navegadores, e é uma das linguagens mais utilizadas no mundo hoje. Com o JS, é possível realizar a comunicação da interface web com o microsserviço, de forma com que a página web possua informações dinâmicas e interativas.

As principais vantagens dessa tecnologia estão descritas nos itens a seguir:

- Portabilidade e fácil implantação em ambientes diversos.
- Total flexibilidade e customização no desenvolvimento
- Comunidade gigantesca, com tutoriais e snippets de fácil acesso.
- Acesso em quase todos os dispositivos - uma interface web pode ser utilizada em qualquer dispositivo que possua um navegador instalado, independente da arquitetura e do sistema operacional.

As principais desvantagens dessa tecnologia são as seguintes:

- O Javascript é interpretado de maneira diferente dependendo do navegador do cliente. Em geral isso foi resolvido nas novas versões de navegadores, porém navegadores &quot;legacy&quot; ou desatualizados ainda sofrem com esse problema. Isso faz com que seja necessário considerar diversos navegadores e diversas versões ao construir o código, não podendo utilizar as funcionalidades mais novas disponíveis no Javascript.
- Dependendo do caso de uso, o &quot;Javascript Vanilla&quot; não oferece as melhores funcionalidades já embutidas, dependendo muito de soluções proprietárias.

## 2 - Definição do broker

Para a escolha da tecnologia de broker, foi considerada a necessidade do broker possuir a funcionalidade de orquestrar a troca de mensagens utilizando o protocolo MQTT.

A tecnologia escolhida para esse propósito foi o **RabbitMQ** , que é a ferramenta de message broker open source mais utilizada no mundo. O RabbitMQ é um software leve e de fácil implantação, sendo possível implantá-lo tanto em ambientes On-Premises quanto em ambientes Cloud. Com o RabbitMQ, é possível utilizar diversos protocolos de comunicação, inclusive o protocolo MQTT, exigido na especificação do projeto.

As principais vantagens do RabbitMQ estão descritas nos itens a seguir:

- Possibilidade de implantar em clusters, obtendo uma alta disponibilidade, resiliência e performance.
- O SDK suporta diferentes linguagens de programação, inclusive a utilizada neste projeto.
- Possui um dashboard interativo, onde é possível visualizar métricas e interagir com as funcionalidades do broker.
- Suporta diversos mecanismos de segurança e autenticação, como TLS e LDAP.

Algumas das desvantagens do RabbitMQ podem ser visualizadas nos itens a seguir:

- Difícil de diagnosticar falhas e configurações incorretas.
- Alguns aspectos são um pouco técnicos

## 3 - Armazenamento

Para a escolha do mecanismo de armazenamento, foi considerada a necessidade de salvar os dados tratados e processados pelo microsserviço. Devido ao fato dos dados serem possivelmente simples, foi escolhido a ferramenta **MongoDB** , um banco de dados não relacional e de fácil implantação e interação.

O MongoDB é um banco de dados de propósito geral, baseado em documentos, criado para desenvolvedores de qualquer tipo de linguagem de programação.

O banco de dados MongoDB armazena dados em documentos do tipo JSON. Hoje, essa é possivelmente a maneira mais comum de transmitir dados, e é muito mais poderosa do que o modelo tradicional de linha/coluna.

O MongoDB possui a flexibilidade de ser implantado em ambientes On-Premises e ambientes Cloud, e também traz funcionalidades de escalabilidade, alta disponibilidade e resiliência.

Alguma das vantagens do uso do MongoDB estão descritas nos itens a seguir:

- Linguagem de consulta rica e expressiva que permite filtrar e classificar por qualquer campo.
- Suporte para agregações, pesquisa geo-baseada, pesquisa gráfica e pesquisa de texto.
- Mecanismos de controle de segurança sofisticado e robusto.
- Suporte a quase todas as linguagens de programação, inclusive a utilizada neste projeto.
- Possibilidade de implantação com alta disponibilidade, resiliência e escalabilidade automática.

As vantagens identificadas dessa ferramenta são descritas nos itens abaixo:

- Devido ao modo com que o MongoDB foi projetado, os joins tendem a ser muito mais lentos do que no banco de dados relacional.
- Para quem tem experiência com banco de dados relacional, a maneira de se pensar pode ser um pouco confusa inicialmente.

## 4 - Micro-serviço - Seleção da linguagem e do framework de desenvolvimento

Para a escolha do mecanismo de armazenamento, foi considerada a necessidade de coordenar outros componentes, gerenciar as inserções no banco de dados, realizar a conexão com o broker MQTT. Para atender os requisitos, foi escolhida a linguagem **Node.JS** , juntamente com a framework **Express.JS**.

O Node.JS é um runtime de Javascript construído em cima da engine Chrome V8. O Node.JS é utilizado para construir aplicações de rede escaláveis. Isso significa que a linguagem pode ser utilizada para criar aplicações Javascript para rodar como uma aplicação standalone em uma máquina, não dependendo de um browser para a execução.

O Express.JS é um framework para aplicativo da web do Node.js mínimo e flexível que fornece um conjunto robusto de recursos para aplicativos web e mobile. Além disso, o Express fornece uma camada fina de recursos fundamentais para aplicativos da web.

Algumas das vantagens do Node.JS estão descritas nos itens abaixo:

- Devido ao fato do Node.JS possuir um gerenciador de pacotes extremamente eficiente e robusto, esta linguagem de programação fornece uma plataforma com potencial para ser utilizada em qualquer situação.
- A velocidade de implantação e replicação de máquinas é extremamente rápida, o que significa menos custo e mais eficiência na construção de aplicações.
- Devido ao fato de interfaces web utilizarem o Javascript (praticamente a mesma linguagem utilizada no Node.JS), há ganhos na reutilização de código e na criação de equipes multidisciplinares, com melhor aproveitamento de recursos.

As desvantagens relacionadas ao Node.JS são as seguintes:

- É possível ocorrer memory-leaks com o Node.JS - estes são piores do que em ambientes de navegador de execução curta, como o JavaScript rodando no navegador.
- O loop de eventos do Node.JS faz com que o tratamento de erros assíncronos seja mais difícil do que o tratamento de erros síncronos.
- É uma linguagem não tipada (não há declarações de tipo de variáveis, como em C ou JAVA). Isso é resolvido com a utilização do TypeScript.

As vantagens em relação à utilização do Express.JS podem ser visualizadas nos itens abaixo:

- O Express.JS é uma framework que disponibiliza uma interface extremamente sucinta para iniciar o desenvolvimento, o que se traduz em pouquíssimo código para iniciar uma aplicação.
- O Express é a framework de Node.JS mais utilizada do mundo, trazendo uma comunidade imensa, com vários plugins e bibliotecas open-source disponíveis.
- A performance obtida no Express.JS é superior à maioria das outras frameworks de Node.JS.
- O Express.JS permite a utilização de qualquer banco de dados.

As desvantagens em relação à utilização do Express.JS estão descritas nos itens abaixo:

- Alguns usuários acreditam que o ExpressJS não é feito para aplicações muito grandes, já que sua eficiência é prejudicada com o crescimento da aplicação.
- O Express.JS exige uma quantidade razoável de configuração no início do projeto, desde autenticação, ORM, validação, rotas, etc...

## 5 - Referências

- [https://gitlab.com/felipetdecarli/kafka-mqtt](https://gitlab.com/felipetdecarli/kafka-mqtt)
- [https://edisciplinas.usp.br/mod/assign/view.php?id=3680277](https://edisciplinas.usp.br/mod/assign/view.php?id=3680277)
- [https://www.mongodb.com/pt-br](https://www.mongodb.com/pt-br)
- [https://www.rabbitmq.com/](https://www.rabbitmq.com/)
- [https://nodejs.org/en/](https://nodejs.org/en/)
- [https://expressjs.com/pt-br/](https://expressjs.com/pt-br/)

![](RackMultipart20210930-4-1ldh88f_html_c11314fdaf014975.gif)
